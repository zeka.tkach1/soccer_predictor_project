import pandas as pd
import click
from sklearn.model_selection import train_test_split
from catboost import CatBoostClassifier

from src.metrics.metric_roi import roi_score

# INPUT_PATH = '../../data/processed/league-matches-4.csv'
# MODEL_PATH = '../../models/'
SEED = 1


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("model_path", type=click.Path())
def train_model(input_path: str, model_path: str) -> float:
    """
    Catboost training function. Saves the trained model to the specified directory

    Parameters
    ----------
    input_path : string
        Path to input train data
    model_path : string
        The path to save the model

    Returns
    -------
    roi : float
        Roi metric for validation data
    """
    df = pd.read_csv(input_path)
    y = df['target']
    X = df.drop(columns='target')
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=0.25, random_state=1)

    params = {'loss_function': 'MultiClass',
              'eval_metric': 'Accuracy',
              # 'n_estimators': 6000,
              'n_estimators': 60,
              'max_depth': 3,
              'eta': 0.6006,
              #   'cat_features': cat_features,
              'task_type': 'GPU',
              'verbose': 500,
              'random_seed': SEED
              }
    cbc_model = CatBoostClassifier(**params)
    cbc_model.fit(X_train, y_train,
                  eval_set=(X_valid, y_valid))
    cbc_model.save_model(model_path, format="cbm")

    return roi_score(X_valid, y_valid, cbc_model)


if __name__ == '__main__':
    train_model()
