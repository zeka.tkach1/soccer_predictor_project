import pandas as pd
import numpy as np
import click
from sklearn.metrics import classification_report
from catboost import CatBoostClassifier

from src.metrics.metric_roi import roi_score

# INPUT_PATH = '../../data/processed/league-matches-4.csv'
# MODEL_PATH = '../../models/'
SEED = 1


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("model_path", type=click.Path(exists=True))
@click.argument("outfile", type=click.Path())
def train_model(input_path: str, model_path: str, outfile: str) -> float:
    """
    Catboost prediction function. Outputs roi score and classification report

    Parameters
    ----------
    input_path : string
        Path to the input data
    model_path : string
        The path to the saved model
    outfile : string
        Path to the save eval data

    Returns
    -------
    roi : float
        Roi metric for validation data
    """
    df = pd.read_csv(input_path)
    y = df['target']
    X = df.drop(columns='target')

    cbc_model = CatBoostClassifier()
    cbc_model.load_model(model_path)
    y_pred = cbc_model.predict(X)
    print(classification_report(y, y_pred))
    np.save(outfile, y_pred)
    return roi_score(X, y, cbc_model)


if __name__ == '__main__':
    train_model()
