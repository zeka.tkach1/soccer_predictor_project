import pandas as pd
from catboost import CatBoostClassifier


def roi_score(x_valid: pd.DataFrame, y_valid: pd.DataFrame, model: CatBoostClassifier) -> float:
    """
    Parameters
    ----------
    x_valid : pd.DataFrame
        Validation features
    y_valid : pd.DataFrame
        Validation targets
    model : catboost.core.CatBoostClassifier
        trained model

    Returns
    -------
    roi : float
        Roi score
    """
    x_roi = x_valid[['odds_ft_1', 'odds_ft_x', 'odds_ft_2']].copy()
    x_roi['target_true'] = y_valid
    x_roi['target_pred'] = model.predict(x_valid)

    x_roi['coef'] = x_roi.loc[x_roi.target_true == x_roi.target_pred].apply(
        lambda row: row.odds_ft_1 if row.target_true == 'home' else
        row.odds_ft_x if row.target_true == 'draw' else
        row.odds_ft_2, axis=1)

    bet = 100
    successful_forecast = x_roi[x_roi.coef.notna()].shape[0]
    unsuccessful_forecast = x_roi[x_roi.coef.isna()].shape[0]
    coef_mean = x_roi[x_roi.coef.notna()].coef.mean()
    income_roi = successful_forecast * bet * coef_mean + unsuccessful_forecast * bet * 0
    investment_roi = (successful_forecast + unsuccessful_forecast) * bet
    roi = (income_roi - investment_roi) / investment_roi
    print('roi:', roi)
    return roi


if __name__ == '__main__':
    roi_score()
