import pandas as pd
import numpy as np
import click

# INPUT_PATH = '../../data/interim/league-matches-2.csv'
# OUTPUT_PATH = '../../data/interim/league-matches-3-posteriori.csv'


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def create_match_data(input_path: str, output_path: str):
    """
    The function removes a posteriori information from the data and creates a target label

    Parameters
    ----------
    input_path: string
        Path to input data
    output_path : string
        The path where to save a posteriori data

    Returns
    -------
    """
    df = pd.read_csv(input_path)
    incomplete_df = df.loc[(df.status == 'incomplete') | (df.status == 'suspended')]
    complete_df = df.loc[df.status == 'complete']
    object_posteriori_columns = ['homeGoals', 'awayGoals']
    bool_posteriori_columns = incomplete_df.select_dtypes(include='bool').columns.values
    float64_posteriori_columns = ['team_a_xg', 'team_b_xg', 'total_xg']

    # a priori data
    int64_priori_columns = ['id', 'homeID', 'awayID', 'roundID',
                            'game_week', 'date_unix', 'btts_potential',
                            'btts_fhg_potential', 'btts_2hg_potential',
                            'o45_potential', 'o35_potential', 'o25_potential',
                            'o15_potential', 'o05_potential', 'o15HT_potential',
                            'o05HT_potential', 'o05_2H_potential', 'o15_2H_potential',
                            'u45_potential', 'u35_potential', 'u25_potential',
                            'u15_potential', 'u05_potential', 'corners_o85_potential',
                            'corners_o95_potential', 'corners_o105_potential',
                            'matches_completed_minimum']

    int64_posteriori_columns = incomplete_df.select_dtypes(include='int64').columns.difference(
        int64_priori_columns).values

    conditions = [(complete_df.winningTeam == complete_df.homeID),
                  (complete_df.winningTeam == complete_df.awayID),
                  (complete_df.winningTeam == -1)]

    choices = ['home', 'away', 'draw']
    complete_df['target'] = np.select(conditions, choices)
    df_target_posteriori = complete_df.drop(np.concatenate([int64_posteriori_columns,
                                                            float64_posteriori_columns,
                                                            object_posteriori_columns,
                                                            bool_posteriori_columns]), axis=1)
    df_target_posteriori.to_csv(output_path, index=False)


if __name__ == '__main__':
    create_match_data()
