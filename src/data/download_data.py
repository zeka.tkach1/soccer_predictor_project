import requests
import pandas as pd
import click

KEY = 'test85g57'
# OUTPUT_PATH = '../../data/raw/league-matches.csv'


@click.command()
@click.argument("output_path", type=click.Path())
@click.argument("key", type=click.STRING)
def download_data(output_path: str, key: str = KEY):
    """
    Downloads data using the API footystats.org/api/

    Parameters
    ----------
    output_path : string
        The path where to upload raw data
    key : string (default: 'test85g57')
        API key for footystats.org

    Returns
    -------
    """
    season_id_list = [1625, 2012, 4759, 6135, 7704]
    df = pd.DataFrame([])

    for season_id in season_id_list:
        d = requests.get(
            f'https://api.football-data-api.com/league-matches?key={key}&season_id={season_id}')
        df = pd.concat([df, pd.json_normalize(d.json()['data'])])
    df.to_csv(output_path, index=False)


if __name__ == '__main__':
    download_data()
