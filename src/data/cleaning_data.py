import pandas as pd
import numpy as np
import click

# INPUT_PATH = '../../data/raw/league-matches.csv'
# OUTPUT_PATH = '../../data/interim/league-matches-2.csv'


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def clean_data(input_path: str, output_path: str):
    """
    The function cleans data from unnecessary features

    Parameters
    ----------
    input_path : string
        Path to input data
    output_path : string
        The path where to save the cleared data

    Returns
    -------
    """
    df = pd.read_csv(input_path)
    object_types_columns = ['season', 'status', 'homeGoals',
                            'awayGoals', 'stadium_name',
                            'home_name', 'away_name']
    bool_types_columns = df.select_dtypes(include='bool').columns.values
    float64_types_columns = df.select_dtypes(include='float64').columns.values

    # categorical features
    # float64_categorical_types_columns = ['refereeID', 'coach_a_ID', 'coach_b_ID']

    # unnecessary features
    int64_trash_types_columns = ['revised_game_week', 'odds_dnb_1', 'odds_dnb_2',
                                 'odds_btts_1st_half_yes', 'odds_btts_1st_half_no',
                                 'odds_btts_2nd_half_yes', 'odds_btts_2nd_half_no',
                                 'no_home_away', 'goalTimingDisabled',
                                 'corner_timings_recorded', 'card_timings_recorded',
                                 'attacks_recorded', 'pens_recorded',
                                 'goal_timings_recorded', ]
    int64_types_columns = df.select_dtypes(include='int64').drop(
        columns=int64_trash_types_columns).columns.values

    df_clear = df[np.concatenate([int64_types_columns, float64_types_columns,
                                  object_types_columns, bool_types_columns])].copy()

    df_clear.to_csv(output_path, index=False)


if __name__ == '__main__':
    clean_data()
