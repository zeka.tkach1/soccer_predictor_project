import pandas as pd
import numpy as np
import click

INPUT_PATH = '../../data/interim/league-matches-3-posteriori.csv'
OUTPUT_PATH = '../../data/processed/league-matches-4.csv'


@click.command()
@click.argument("input_path", type=click.Path(exists=True))
@click.argument("output_path", type=click.Path())
def process_features(input_path: str, output_path: str):
    """
    The function adds new categorical features and transforms them by unitary encoding.
    The function also does the factorization of the target.
    0 - home
    1 - away
    2 - draw

    Parameters
    ----------
    input_path: string
        Path to input data
    output_path : string
        The path where to save the processed data

    Returns
    -------
    """
    df = pd.read_csv(input_path)

    # fill NA
    df[df.columns[df.isna().any()].values] = df[df.columns[df.isna().any()].values].fillna(0)

    # float to int
    float_to_int_col_list = ['refereeID', 'coach_a_ID', 'coach_b_ID']
    df[float_to_int_col_list] = df[float_to_int_col_list].astype(np.int64)

    # factorize target
    df['target'] = pd.factorize(df.target)[0]

    # add cat features
    cats = ['matches_completed_minimum', 'refereeID', 'coach_a_ID', 'coach_b_ID', 'homeID',
            'awayID']
    df[cats] = df[cats].astype(object)

    # del target for one-hot enc
    one_hot_cat = df.select_dtypes('object').columns

    df = pd.get_dummies(df, prefix=one_hot_cat)

    df.to_csv(output_path, index=False)


if __name__ == '__main__':
    process_features()
